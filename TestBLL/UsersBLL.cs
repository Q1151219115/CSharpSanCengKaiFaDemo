﻿using System.Linq;
using Model;
using IDAL;
using IBLL;

namespace TestBLL
{
    /// <summary>
    /// Users业务逻辑实现
    /// </summary>
    public class UsersBLL : BaseBLL<Users>, IUsersIBLL
    {
        private static IUsersIDAL dal = new DALFactory<Users>().CreateDAL();

        //构造函数中将具体的数据访问对象传到父级的构造函数中
        public UsersBLL() : base(dal)
        {
        }

        public IQueryable<Users> Get(int pageSize, int pageIndex)
        {
            return dal.Select(pageSize, pageIndex);
        }
    }
}
