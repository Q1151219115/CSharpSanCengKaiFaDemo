﻿using Model;
using IDAL;
using IBLL;

namespace TestBLL
{
    /// <summary>
    /// Nation业务逻辑实现
    /// </summary>
    public class NationBLL : BaseBLL<Nation>, INationIBLL
    {
        private static INationIDAL dal = new DALFactory<Nation>().CreateDAL();
        public NationBLL() : base(dal)
        {
        }
    }
}
