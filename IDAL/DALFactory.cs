﻿using System.Configuration;
using System.Reflection;
using Model;

namespace IDAL
{
    /// <summary>
    /// 数据访问类泛型工厂
    /// </summary>
    /// <typeparam name="T">需要操作的Model实体类型</typeparam>
    public class DALFactory<T>
    {
        private static readonly string DAL_NameSpace = ConfigurationManager.AppSettings["DAL_NameSpace"];
        private static readonly string DAL_Users = ConfigurationManager.AppSettings["DAL_Users"];
        private static readonly string DAL_Nation = ConfigurationManager.AppSettings["DAL_Nation"];

        public dynamic CreateDAL()
        {
            if (typeof(T) == typeof(Users))
                return Assembly.Load(DAL_NameSpace).CreateInstance(DAL_NameSpace + "." + DAL_Users) as IUsersIDAL;
            else if (typeof(T) == typeof(Nation))
                return Assembly.Load(DAL_NameSpace).CreateInstance(DAL_NameSpace + "." + DAL_Nation) as INationIDAL;

            return null;
        }
    }
}
