﻿using System.Linq;
using Model;

namespace IDAL
{
    public interface IUsersIDAL : IBaseIDAL<Users>
    {
        //扩展方法描述写在这里

        /// <summary>
        /// 扩展分页方法
        /// </summary>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <returns></returns>
        IQueryable<Users> Select(int PageSize, int PageIndex);
    }
}
