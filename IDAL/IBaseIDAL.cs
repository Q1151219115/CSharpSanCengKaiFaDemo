﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace IDAL
{
    /// <summary>
    /// 数据访问基础描述接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseIDAL<T> where T : class
    {
        IQueryable<T> Select();

        IQueryable<T> Select(Expression<Func<T, bool>> exp);

        T Select(int key);

        int Insert(T entity);

        int Update(T entity);

        int Delete(T entity);
    }
}
