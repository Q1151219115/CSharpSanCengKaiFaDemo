﻿using System;
using Model;
using IBLL;

namespace WebUI
{
    public partial class remove : System.Web.UI.Page
    {
        IUsersIBLL userBLL = new BLLFactory<Users>().CreateBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = Request["id"];
                userBLL.Remove(Convert.ToInt32(id));
                Response.Redirect("page1.aspx");
            }
        }
    }
}