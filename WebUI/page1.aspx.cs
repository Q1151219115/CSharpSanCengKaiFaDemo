﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using IBLL;
using Model;

namespace WebUI
{
    public partial class page1 : System.Web.UI.Page
    {
        //通过业务逻辑层工厂创建相应的操作对象
        IUsersIBLL userBLL = new BLLFactory<Users>().CreateBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //调用对象方法
                Repeater1.DataSource = userBLL.Get().ToList();
                Repeater1.DataBind();
            }
        }
    }
}