﻿using System.Data.Entity;
using Model;

namespace TestDAL
{
    /// <summary>
    /// 上下文对象单例工厂
    /// </summary>
    public class DbContextFactory
    {
        private static DbContext _Test1Context;

        public static DbContext Test1Context
        {
            get
            {
                if (_Test1Context == null) _Test1Context = new Test1DbContext();
                return _Test1Context;
            }
        }
    }
}
