﻿using System;
using Model;
using IDAL;
using System.Linq.Expressions;

namespace TestDAL
{
    public class NationDAL : BaseDAL<Nation>, INationIDAL
    {
        /// <summary>
        /// 重写主键匹配抽象方法
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override Expression<Func<Nation, bool>> GetByKey(int key)
        {
            return r => r.NationCode == ("N" + key.ToString("000"));
        }
    }
}
