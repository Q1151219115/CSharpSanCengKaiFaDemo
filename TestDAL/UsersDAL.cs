﻿using System;
using Model;
using IDAL;
using System.Linq;
using System.Linq.Expressions;

namespace TestDAL
{
    /// <summary>
    /// Users数据访问类
    /// </summary>
    public class UsersDAL : BaseDAL<Users>, IUsersIDAL
    {
        /// <summary>
        /// 重写主键匹配抽象方法
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override Expression<Func<Users, bool>> GetByKey(int key)
        {
            return r => r.Ids == key;
        }

        /// <summary>
        /// 扩展分页方法实现
        /// </summary>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <returns></returns>
        public IQueryable<Users> Select(int PageSize, int PageIndex)
        {
            return context.Set<Users>().Skip((PageIndex - 1) * PageSize).Take(PageSize);
        }

    }
}
