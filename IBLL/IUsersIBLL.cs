﻿using System.Linq;
using Model;

namespace IBLL
{
    /// <summary>
    /// Users业务逻辑描述接口
    /// </summary>
    public interface IUsersIBLL : IBaseIBLL<Users>
    {
        /// <summary>
        /// 扩展分页方法描述
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        IQueryable<Users> Get(int pageSize, int pageIndex);
    }
}
