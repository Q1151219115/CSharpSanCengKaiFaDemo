﻿using Model;

namespace IBLL
{
    /// <summary>
    /// Nation业务逻辑描述接口
    /// </summary>
    public interface INationIBLL : IBaseIBLL<Nation>
    {
    }
}
