﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace IBLL
{
    /// <summary>
    /// 业务逻辑基础描述接口
    /// </summary>
    public interface IBaseIBLL<T>
    {
        IQueryable<T> Get();

        IQueryable<T> Get(Expression<Func<T, bool>> exp);

        T Get(int id);

        int Add(T entity);

        int Change(T entity);

        int Remove(int id);
    }
}
