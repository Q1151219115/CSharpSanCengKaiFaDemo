﻿using Model;
using System.Configuration;
using System.Reflection;

namespace IBLL
{
    /// <summary>
    /// 业务逻辑工厂类
    /// </summary>
    /// <typeparam name="T">需要操作的业务逻辑类Model实体类型</typeparam>
    public class BLLFactory<T>
    {
        private static readonly string BLL_NameSpace = ConfigurationManager.AppSettings["BLL_NameSpace"];
        private static readonly string BLL_Users = ConfigurationManager.AppSettings["BLL_Users"];
        private static readonly string BLL_Nation = ConfigurationManager.AppSettings["BLL_Nation"];

        /// <summary>
        /// 返回此泛型Model对应的BLL 
        /// </summary>
        /// <returns></returns>
        public dynamic CreateBLL()
        {
            if (typeof(T) == typeof(Users))
                return Assembly.Load(BLL_NameSpace).CreateInstance(BLL_NameSpace + "." + BLL_Users) as IUsersIBLL;
            else if (typeof(T) == typeof(Nation))
                return Assembly.Load(BLL_NameSpace).CreateInstance(BLL_NameSpace + "." + BLL_Nation) as INationIBLL;

            return null;
        }
    }
}
